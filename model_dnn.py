#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  8 10:44:45 2017

@author: markus
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import pandas as pd

from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler
from Scaler import Rescaler, MinMax
#import decimal
tf.logging.set_verbosity(tf.logging.INFO)
#import os

#from sklearn.externals import joblib

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)


#data_ = pd.read_csv('Coba_Blanjaclean.csv')
data_ = pd.read_csv('Coba_Lokamediaclean.csv')



#==============================================================================
model_path = "/home/markus/Music/MLP/DNN"

"""Model DNN untuk prediksi Click hari (h+1) berdasarkan metrik hari (h)
model dengan dua hidden layer, default nodes 256 dan 512
"""

class DNN(object):


    def __init__(self, numOfFeatures, numOfLabels, nodes):

        tf.reset_default_graph()

        # Number of Features and Labels

        self.numOfFeatures = numOfFeatures
        self.numOfLabels = numOfLabels
        self.nodes = nodes

        # input placeholder, dimension [None, numOfFeatures]
        self.X = tf.placeholder(tf.float32, shape=[None, self.numOfFeatures], name="Features")
        # true target placeholder
        self.Y_ = tf.placeholder(tf.float32, shape=[None, self.numOfLabels], name="Outputs")

        #np.random.seed(seed=1234)

        # number of Neuron in each hidden layer
        self.nodes1 = nodes[0]
        self.nodes2 = nodes[1]
        self.nodes3 = nodes[2]
        #self.nodes3 = nodes3
        eps = 0.1

        # first layer weight and bias
        self.W1 = tf.Variable(tf.random_uniform([self.numOfFeatures, self.nodes1], minval=0, maxval=eps), name="W1")
        self.B1 = tf.Variable(tf.random_uniform([self.nodes1], minval=0, maxval=eps), name="B1")

        self.W2 = tf.Variable(tf.random_uniform([self.nodes1, self.nodes2], minval=0, maxval=eps), name="W2")
        self.B2 = tf.Variable(tf.random_uniform([self.nodes2], minval=0, maxval=eps), name="B2")

        self.W3 = tf.Variable(tf.random_uniform([self.nodes2, self.nodes3], minval=0, maxval=eps), name="W3")
        self.B3 = tf.Variable(tf.random_uniform([self.nodes3], minval=0, maxval=eps), name="B3")


        # last layer weight and bias
        self.W_last = tf.Variable(tf.random_uniform([self.nodes2, self.numOfLabels], minval=0, maxval=eps), name="W_last")
        self.B_last = tf.Variable(tf.random_uniform([self.numOfLabels], minval=0, maxval=eps), name="B_last")

        # activation functions
        self.Y1 = tf.nn.relu(
            tf.add(
                tf.matmul(self.X, self.W1, name="Mul1"), self.B1,
                name="Add1"
            ),
            name="AF1"
        )

        self.Y2 = tf.nn.relu(
            tf.add(
                tf.matmul(self.Y1, self.W2, name="Mul2"), self.B2,
                name="Add2"
            ),
            name="AF2"
        )

        self.Y3 = tf.nn.relu(
            tf.add(
                tf.matmul(self.Y2, self.W3, name="Mul3"), self.B3,
                name="Add3"
            ),
            name="AF2"
        )

        self.Y = tf.add(
            tf.matmul(self.Y2, self.W_last, name="Mul_last"), self.B_last,
            name="Add_last"
        )

        # loss function
        self.loss = tf.sqrt(
            tf.reduce_mean(
                tf.squared_difference(self.Y, self.Y_, name="SquaredDiff"),
                name="ReducedMean"
            ),
            name="SquareRoot"
        )


    def train(self, x_t, y_t, steps, learning_rate=0.01, file_dir='./model/CobaDNN'):
        """x_t and y_t are normalized to [0,1]"""
        self.steps = steps

        self.optimizer = tf.train.AdagradOptimizer(learning_rate)
        self.global_step = tf.Variable(0, trainable=False, name="global_step")
        self.train_step = self.optimizer.minimize(self.loss, global_step=self.global_step,
            var_list=[self.W1, self.B1, self.W2,self.B2, self.W_last, self.B_last])

        init = tf.global_variables_initializer()
        self.saver = tf.train.Saver()

        with tf.Session() as sess:
             writer = tf.summary.FileWriter("./Graph", sess.graph)
             sess.run(init)
             for ep in range(steps):
                 sess.run(self.train_step, feed_dict={self.X: x_t, self.Y_ : y_t})
                 if ep % 500 == 0:
                    mse = self.loss.eval(feed_dict={self.X: x_t, self.Y_: y_t})
                    print('Epoch saat ini:', ep, "\tMSE:", mse)
             save_path = self.saver.save(sess, file_dir, write_meta_graph=True)
             print("Model disimpan pada : %s" % save_path)

             writer = tf.summary.FileWriter("./Graph", sess.graph)

             writer.close()


    def testing(self, X_test, y_test, K_ID, max_val, file_dir='./model'):
        #graph = tf.Graph()
#==============================================================================
#
#         if max_:
#             r = Resik(data_)
#             a = r.maxklik(K_ID)
#         else:
#             a = 1.
#==============================================================================

        with tf.Session() as sess:
             #ckpt = tf.train.get_checkpoint_state('./model/')
             graph = tf.get_default_graph()
             self.n_y_test = len(y_test)
             #self.saver = tf.train.import_meta_graph('./model')
             self.saver.restore(sess, tf.train.latest_checkpoint(file_dir))
             #self.saver.restore(sess, '/model/modeldnn.meta')


             self.X = graph.get_tensor_by_name("Features:0")
             self.Y_ = graph.get_tensor_by_name("Outputs:0")
             op_to_restore = graph.get_tensor_by_name("Add_last:0")
             self.P = sess.run(op_to_restore, feed_dict={self.X: X_test})
             for i,n in enumerate(self.P):
                 if n<0:
                     self.P[i]=0

             mse_t = self.loss.eval(feed_dict={self.X: X_test, self.Y_ : y_test})
#==============================================================================
#              for i in range(len(X_test)):
#                  mse = self.loss.eval(feed_dict={self.X: X_test, self.Y_ : y_test})
#                  print ("MSE:", mse)
#==============================================================================
        print ("Selisih y_test dengan prediksi :", abs(self.P-y_test)*max_val)
        print ('Banyak data_prediksi:', len(self.P))
        print ('Banyak data testing:', len(y_test))
        print ("MSE_Prediksi : %s" % mse_t)

        return #self.P





    def predict(self, K_ID, X_p, file_dir='./model'): #shape X_p = (1,8)
        """X_p is normalized metric"""


        self.saver = tf.train.Saver()

        with tf.Session() as sess:
             #ckpt = tf.train.get_checkpoint_state('./model/')
             #n_y_test = len(y_test)
             #self.saver = tf.train.import_meta_graph('./model')
             self.saver.restore(sess, tf.train.latest_checkpoint(file_dir))
             #self.saver.restore(sess, '/model/modeldnn.meta')
             graph = tf.get_default_graph()

             self.X = graph.get_tensor_by_name("Features:0")
             self.Y_ = graph.get_tensor_by_name("Outputs:0")
             op_to_restore = graph.get_tensor_by_name("Add_last:0")
             self.Pr = sess.run(op_to_restore , feed_dict={self.X: X_p}).reshape(1)
        print ('scaled_prediction:', self.Pr)
        print (type(self.Pr))
        print (self.Pr.shape)
        
        l = []
        for i in self.Pr:
            if i <= 0.:
              l.append(0.)
            else:
                l.append(i)
            
        return np.array(l)



    def real_pred(self, y_test, max_val): #max_val = maximum Clicks value from HD = 654.0
#==============================================================================
#
#         if max_:
#             r = Resik(data_)
#             max_val = r.maxklik(K_ID)
#         else:
#             max_val = 1.
#==============================================================================

        l = list(self.P.reshape(len(self.P),))
        self.a = []
        for i in l:
            self.a.append(round(i*(max_val),0))

        print (y_test.shape)

        y = list(y_test.reshape(self.n_y_test,)) # without looping
        self.b = []
        for j in y:
            self.b.append(j*(max_val))

        print ("List Prediksi: %s" % self.a)

        print ("List y_test: %s" % self.b)

        return


    def plot(self, fig_name):

        fig = plt.figure()
        plt.title("Prediksi vs Aktual", fontsize=14)
        plt.plot(self.b, "b.", markersize=10, label="Aktual")
        #plt.plot(pd.Series(np.ravel(Y_test)), "w*", markersize=10)
        plt.plot(self.a, "r.", markersize=10, label="Prediksi")
        plt.legend(loc="upper left")
        plt.ylabel("Aktual")
        plt.xlabel("Hari/metrik")

        plt.show()
        fig.savefig(fig_name, dpi=400)
        return

    def errorbar(self, fig_name):
        fig = plt.figure()
        plt.title("Ploting with mean Error")
        v = []
        w = []
        for i,j in enumerate(self.a):
            if j > self.b[i]:
                v.append(j-self.b[i])
            else:
                v.append(0.)
        for i,j in enumerate(self.a):
            if j < self.b[i]:
                w.append(self.b[i]-j)
            else:
                w.append(0.)
        print ('panjang v =',len(v), 'panjang w =', len(w))
        print ('v =', v)
        print ('w =', w)
        self.upper_error = [sum(v)/len(v) for i in v]
        self.lower_error = [sum(w)/len(w) for i in w]
        print ('lower_error_mean :', self.lower_error[0])
        print ('upper_error_mean :', self.upper_error[0])
        a_err = [self.lower_error, self.upper_error]
        x = np.arange(0, len(self.a), 1)
        print ("panjang x :", len(x))
        print ("panjang a :", len(self.a))

        plt.plot(self.b, "b.", markersize=10, label="Aktual")
        plt.errorbar(x, self.a, yerr=a_err, fmt='o', mfc='red', mew=0.75, label="Prediksi")
        plt.ylabel("Prediksi")
        plt.xlabel("Hari/metrik")
        plt.legend(loc='upper left')

        plt.show()
        fig.savefig(fig_name, dpi=400)

        return

    def error_plot(self, max_click, fig_name): #plot prediksi min aktual

        fig = plt.figure()
        plt.title("error", fontsize=14)
        self.l = [x - y for (x,y) in zip(self.a, self.b)]
        plt.ylim(-1.01*max_click, 1.01*max_click)
        plt.plot(self.l, "r.", markersize=10, label="error")
        plt.plot([0, len(self.P)], [self.upper_error, self.upper_error], 'g--', lw=1)
        plt.plot([0, len(self.P)], [0, 0], 'b--', lw=1)
        plt.plot([0, len(self.P)], [[-1.*i for i in self.lower_error], [-1.*i for i in self.lower_error]], 'g--', lw=1)

        plt.plot([0, len(self.P)], [0.1*max_click, 0.1*max_click], 'r--', lw=1)
        plt.plot([0, len(self.P)], [-0.1*max_click, -0.1*max_click], 'r--', lw=1)

        plt.legend(loc="upper left")
        plt.xlabel("Hari/metrik")
        plt.ylabel("Error")
        
        plt.show()
        fig.savefig(fig_name, dpi=400)
        self.u = len(self.l)

        #mse_p = np.sqrt((self.P-y_test)**2/len(y_test))
        return #"MSE_P : ", mse_p
    
    def persen(self, max_click):
        #b = len(self.l)
        a = []
        for i in self.l:
            if i < self.upper_error[0] and i > - self.lower_error[0]:
              a.append(i)
        b = []
        for i in self.l:
            if i < 0.1*max_click and i > -0.1*max_click:
                b.append(i)

        return len(a)/len(self.l), len(b)/len(self.l)


        #plt.plot(self.a, self.b, 'r-', lw=5)
        #plt.text(-1, 1.2, 'Save Loss=%.4f' % l, fontdict={'size': 15, 'color': 'red'})






#X_p = np.array([50, 1500., 0.5, 0.7, 0.7, 0.75, 0., 0.7, 3., 4., 6.])
#print (X_p)
"""Rescaling processes untuk X_p
input : numpy array metric
output : numpy array scaled metric"""

#a = Rescaler(data_,'14217946351')
#a.Max_m()
#a.Min_m()
#y_p = a.rescaling(X_p)















