#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 31 11:11:57 2017

@author: markus
"""

#X_p = np.array(['Conversions','Cost', 'Search Impr. share', 'Landing page experience', 'Ad relevance', 'Quality score', 'Bounce rate','CTR', 'Max. CPC', 'day_of_week'])
#X_max = ['m1', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'm9', 'm10']

import numpy as np
import pandas as pd


X_p = np.array([4.,7.,2.,8.,6.,7.,4.,9.,6.,5., 3., 5., 7., 4., 3., 2.,1.]) #nilai metrik hari-H
X_max = [1., 5., 6., 7., 8., 9., 3., 4.8, 6., 5.] #list_nilai_maksimal untuk tiap metrik
X_min = [1., 3., 0., 1., 1., 0., 2., 4., 5., 3.] #list_nilai_minimum untuk tiap metrik


nama = 'Coba_Lokamediaclean.csv'
data_ = pd.read_csv(nama)
print ('Data yang digunakan adalah '+ nama)



class Rescaler(object):
    
    def __init__(self, data, metrics, K_ID):
        self.K_ID = K_ID
        self.data = data[data['Keyword ID']==K_ID]
        self.metrics = metrics
      
    
    def minmax(self):
        ma = []
        mi = []
        for i in self.metrics:
          ma.append(self.data[i].max())
          mi.append(self.data[i].min())
        return ma, mi
    
    def rescaling(self, X):
      """Input X = array of unscaled data
      Output = array scaled Output data"""
      
        a, b = Rescaler(self.data, self.metrics, self.K_ID).minmax()
        X_p = zip(a,b)
      
        X_max_s = []
        for i,j in enumerate(X):
          if j == a[i] or j >= a[i]:
            k = 1.
          elif j == b[i] or j <= b[i]:
            k = 0.
          else:
            k = j / a[i]
        
          X_max_s.append(k)
        return np.array(X_max_s)    
      
col_names = ['Avg. CPC', 'Conversions', 'Clicks', 'Cost', 'Impressions', 'Search Impr. share', 'Landing page experience',\
  'Ad relevance', 'Quality score', 'Bounce rate', 'CTR', 'Max. CPC', 'Expected clickthrough rate', 'Month of Year', 'Day of week', 'Day of the month', 'Week of the month']

#testing scaler
a = Rescaler(data_, col_names, 3535566232)
x,y = a.minmax()

X_s = [3604750000,6.0,22.0,100933000000.0,204.0,1.0,0.75,0.75,10.0,0.0,1.0,1500000000.0,0.75,6.0,6.0,40.0,4.0]


print ('Scaled metrics : ', a.rescaling(X_s))






      
def minmax_scaler(data, *list_metric):
  columns_name = list(data.columns)
  
  for j in list_metric:
    if data[data.columns[j]].max() == data[data.columns[j]].min():
      data[data.columns[j]]=0.
    elif data[data.columns[j]].max()!= data[data.columns[j]].min():
      data[data.columns[j]] = data[data.columns[j]]/data[data.columns[j]].max()
  return data



f = open("guru100.txt","w+")
for i in range(10):
    f.write(str(i+1))
        #f.write("This is line %d\r\n" % (i+1))
f.close()
  

#==============================================================================

def get_maxval(K_ID):
    a = []
    with open('./'+K_ID+'/y_testing_'+K_ID+".txt","r") as myfile:
         lines = myfile.readlines()
         for i in lines:
             a.append(float(i.replace('\n','')))
    #print ('[Clicks, Conversions, Cost, SIS, LPE, AdR, QS, BR, CTR, Month of Year, Day of week]')
    return a








