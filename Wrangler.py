#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 01:57:58 2017

@author: markus
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import pandas as pd
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
#from sklearn.preprocessing import MinMaxScaler 
from Scaler import MinMax
tf.logging.set_verbosity(tf.logging.INFO)
import os

#from sklearn.externals import joblib


"""data_ merupakan data input"""


#data_ = pd.read_csv('misterclean.csv')
#nama = 'Coba_Lokamediaclean.csv'
#data_ = pd.read_csv(nama)
#print ('Data yang digunakan adalah '+ nama)


class Resik(object):
    """Class resik dengan input dataframe"""
    
    def __init__(self, data_):
        self.data_ = data_
    
    def ngresiki(self):
        self.data_ = self.data_[(self.data_.Keyword != '#NAME?') & (self.data_.Keyword != '#REF!') & (self.data_.Keyword != '#VALUE!')]
        self.data_['Keyword ID'] = self.data_['Keyword ID'].astype(str)

        print (self.data_.columns)
        
        print ("=======================================================================>>>")
        self.data_ = self.data_.sort_values(by='Day')
        self.data_.head(10)

        #self.data_ = self.data_[['Day','Keyword ID', 'Keyword','Clicks','Conversions','Cost','Search Impr. share', 'Landing page experience', 'Ad relevance', 'Quality score', 'Bounce rate','CTR', 'Max. CPC','day_of_week' ]]
        self.data_ = self.data_[['Day', 'Keyword ID','Keyword','Clicks','Conversions','Cost','Impressions', 'Search Impr. share', 'Landing page experience', 'Ad relevance', 'Quality score', 'Bounce rate','CTR','Month of Year','Day of week', 'Day of the month']]

        self.data_ = self.data_.rename(columns={'Keyword ID': 'Keyword_ID'})
        return self.data_
    
        
    
    def daftar_K(self):
        """List keyword"""
        y = list(np.unique(self.data_['Keyword']))
        return y
    
    
    def daftar_KID(self):
        """List keyword_ID"""
        a = list(self.data_.Keyword_ID.unique()) 
        return a
    
#class KID(Resik):

    def hd_KID(self, K_ID, metric):
        """Input : Keyword_ID
        Output : Historical data untuk Keyword_ID tertentu """
        if metric=='Clicks':
          df = self.data_[self.data_.Keyword_ID==K_ID]
          df.Clicks = df.Clicks.shift(-1)
          df = df[:-1]
        elif metric=='Impressions':
          df = self.data_[self.data_.Keyword_ID==K_ID]
          df.Impressions = df.Impressions.shift(-1)
          df = df[:-1]
        elif metric=='Conversions':
          df = self.data_[self.data_.Keyword_ID==K_ID]
          df.Conversions = df.Conversions.shift(-1)
          df = df[:-1]
        
        return df

#==============================================================================
# data1 = data_[data_.Keyword_ID == '159913000000.0']
# data1.Clicks = data1.Clicks.shift(-1)
# data1 = data1[:-1]
#==============================================================================

#X = data_.drop('Clicks', axis=1)
    def hd_EKD(self, daftar):
        
        for i in daftar:
            self.data = self.data_[self.data_.Keyword_ID==i]
            self.data.Clicks = self.data.Clicks.shift(-1)
            self.data = self.data[:-1]
        return self.data
           

    def data_train(self, K_ID, metric, test_size=0.2):
        """Spltting the data into 0.8 data train and 0.2 for test data,
        Input : Data untuk suatu Keyword_ID tertentu
        Output : Data train dalam bentuk array"""
        data1 = self.data_[self.data_.Keyword_ID == K_ID]
    
        #X_f = data1[['Conversions','Cost','Search Impr. share', 'Landing page experience', 'Ad relevance', 'Quality score', 'Bounce rate','CTR', 'Max. CPC', 'day_of_week']]
        X_f = data1[['Conversions','Cost','Search Impr. share', 'Landing page experience', 'Ad relevance', 'Quality score', 'Bounce rate','CTR','Month of Year','Day of week', 'Day of the month']]
        
        if metric=='Clicks':
          y_f = data1[['Clicks']]
          X_ = MinMax(X_f).scaler()
          y_ = MinMax(y_f).scaler()
          x = X_.values
          y = y_.values
        
          X_tr = pd.DataFrame(x)
          X_train, self.X_test, y_train, self.y_test = train_test_split(X_tr, y, test_size=0.2, random_state=42)
          X_train.columns = range(X_train.shape[1])  
        
          X_train = X_train.reset_index(drop=True).as_matrix()
          y_train = y_train.reshape(len(X_train),1)
          
        elif metric=='Impressions':
          y_f = data1[['Impressions']]
          X_ = MinMax(X_f).scaler()
          y_ = MinMax(y_f).scaler()
          x = X_.values
          y = y_.values
        
          X_tr = pd.DataFrame(x)
          X_train, self.X_test, y_train, self.y_test = train_test_split(X_tr, y, test_size=0.2, random_state=42)
          X_train.columns = range(X_train.shape[1])  
        
          X_train = X_train.reset_index(drop=True).as_matrix()
          y_train = y_train.reshape(len(X_train),1)
        
        elif metric=='Conversions':
          y_f = data1[['Clicks']]
          X_ = MinMax(X_f).scaler()
          y_ = MinMax(y_f).scaler()
          x = X_.values
          y = y_.values
        
          X_tr = pd.DataFrame(x)
          X_train, self.X_test, y_train, self.y_test = train_test_split(X_tr, y, test_size=0.2, random_state=42)
          X_train.columns = range(X_train.shape[1])  
        
          X_train = X_train.reset_index(drop=True).as_matrix()
          y_train = y_train.reshape(len(X_train),1)
          
        return X_train, y_train
    
    def data_test(self):
        self.X_test.columns = range(self.X_test.shape[1])  
        #self.y_test.columns = self.y_test
        X_test = self.X_test.reset_index(drop=True).as_matrix()
        y_test = self.y_test.reshape(len(X_test),1)
        return X_test, y_test
    
    def maxvalue(self, K_ID, metric):
        data1 = self.data_[self.data_.Keyword_ID == K_ID]
        if metric=='Clicks':
          y = data1.pop('Clicks')
        elif metric=='Impressions':
          y = data1.pop('Impressions')
        elif metric=='Conversions':
          y = data1.pop('Conversions')
        return y.max()
    


def maxval(df, K_ID, metric):
  """take the maximum value"""
  a = Resik(df).ngresiki()
  a[a['Keyword_ID']==K_ID]
  if metric=='Clicks':
    return a['Clicks'].max()
  elif metric=='Impressions':
    return a['Impressions'].max()
  elif metric=='Conversions':
    return a['Conversions'].max()
    
  
    
"""    
    
X = data1[['Conversions','Cost','Search Impr. share', 'Landing page experience', 'Ad relevance', 'Quality score', 'Bounce rate','CTR', 'Max. CPC']]
y = data1.pop('Clicks')

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

X_train.columns = range(X_train.shape[1]) #mengubah nama kolom data train dengan index 0 sampai 8 
y_train.columns = y_train

X_train = X_train.reset_index(drop=True).as_matrix() #reset index dan mengubah ke matrix
#X_train = X_train.reshape()


y_train = y_train.reset_index(drop=True).reshape(len(X_train),1)
X_test = X_test.reset_index(drop=True).as_matrix()
y_test = y_test.reset_index(drop=True).as_matrix()

"""
r = Resik(data_)
r.ngresiki()
r.daftar_K()
list_kw = r.daftar_KID()
hd_klik = r.hd_KID('290543145595','Clicks')
hd_impresi = r.hd_KID('290543145595','Impressions')
hd_konversi = r.hd_KID('290543145595','Conversions')


""""
import matplotlib.pyplot as plt 

#figure, axes = plt.subplots(nrows=2, ncols=2) 

metrik = ['Clicks', 'Impressions', 'Conversions']

for i in metrik:
  l = data_[i]
  x = list(range(len(l[0:101])))
  y = list(l[0:101])
  plt.ylabel(i)
  plt.xlabel('Day')
  plt.title('Plot '+i+' terhadap hari')
  plt.plot(x, y, 'o')
  plt.show()

"""


    
    
    
