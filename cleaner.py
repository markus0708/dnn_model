#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tues Jan 4 14:48:58 2018

@author: markus
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import pandas as pd
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
#from sklearn.preprocessing import MinMaxScaler 
from Scaler import minmax_scaler
tf.logging.set_verbosity(tf.logging.INFO)
import os


#data_ = pd.read_csv('misterclean.csv')
nama = 'Coba_Lokamediaclean.csv'
data_ = pd.read_csv(nama)
print ('Data yang digunakan adalah '+ nama)

#all_metrics
metrics = ['Day', 'Keyword ID', 'Keyword', 'Clicks', 'Conversions', 'Cost', 'Impressions', 'Search Impr. share', 'Landing page experience',\
            'Ad relevance', 'Quality score', 'Bounce rate', 'CTR', 'Month of Year', 'Day of week', 'Day of the month']
  
# list_sp = Keyword_ID dengan KPI   
list_sp = ['K_ID', 'Clicks']

#dropped_columns to get the features data
dropped_metrics = ['Day', 'Keyword_ID', 'Keyword'] + [list_sp[1]]


def clean(data, metrics):
  data_ = data[(data.Keyword != '#NAME?') & (data.Keyword != '#REF!') & (data.Keyword != '#VALUE!')]
  data_['Keyword ID'] = data_['Keyword ID'].astype(str)
  data_ = data_.sort_values(by='Day')
  data_ = data_[metrics]
  data_ = data_.rename(columns={'Keyword ID': 'Keyword_ID'})
  return data_


class Cleaner(object):
    """Class Cleaner dengan input dataframe"""
    
    def __init__(self, data):
        self.data = data
        self.columns = list(data.columns)
    
    def clean(self):
        data_ = self.data[(self.data.Keyword != '#NAME?') & (self.data.Keyword != '#REF!') & (self.data.Keyword != '#VALUE!')]
        data_['Keyword ID'] = data_['Keyword ID'].astype(str)
        data_ = data_.sort_values(by='Day')
        data_ = data_[self.columns]
        data_ = data_.rename(columns={'Keyword ID': 'Keyword_ID'})
        return data_
    
    @property
    def list_keyword(self):
        """List keyword"""
        y = list(np.unique(self.clean()['Keyword']))
        return y
    
    @property
    def list_KID(self):
        """List keyword_ID"""
        a = list(self.clean().Keyword_ID.unique()) 
        return a
    
    
    def hd_KID(self, list_sp):
        """Input : list_sp = ['Keyword_ID', 'KPI']
                   e.g. = list_sp = ['310921431108','Clicks']
        Output : Historical data untuk Keyword_ID tertentu """
        data = self.clean()
        if list_sp[1]=='Clicks':
          df = data[data.Keyword_ID==list_sp[0]]
          df.Clicks = df.Clicks.shift(-1)
          return  df[:-1]
        elif list_sp[1]=='Impressions':
          df = data[data.Keyword_ID==list_sp[0]]
          df.Impressions = df.Impressions.shift(-1)
          return df[:-1]
        elif list_sp[1]=='Conversions':
          df = data[data.Keyword_ID==list_sp[0]]
          df.Conversions = df.Conversions.shift(-1)
          return df[:-1]
        

    def data_train(self, list_sp, drop_metric, test_size=0.2, random_state=42):
        """Spltting the data into 0.8 data train and 0.2 for test data,
        Input : Data untuk suatu Keyword_ID tertentu
        Output : Data train dalam bentuk array"""
        clean_data = self.clean()
        data = clean_data[clean_data.Keyword_ID == list_sp[0]]
        #X_f = data[self.metrics]
        
        if list_sp[1]=='Clicks':
          
          scaled_data = minmax_scaler(data)
          X_f = scaled_data.drop(drop_metric, axis=1)
          y_f = scaled_data[['Clicks']]
          x = X_f.values
          y = y_f.values
        
          X_tr = pd.DataFrame(x)
          X_train, X_test, y_train, y_test = train_test_split(X_tr, y, test_size=0.2, random_state=42)
          X_train.columns = range(X_train.shape[1])
          X_train = X_train.reset_index(drop=True).as_matrix()
          y_train = y_train.reshape(len(X_train),1)
          X_test.columns = range(X_test.shape[1]) 
          X_test = X_test.reset_index(drop=True).as_matrix()
          y_test = y_test.reshape(len(X_test),1)
          
          
        elif list_sp[1]=='Impressions':
          
          scaled_data = minmax_scaler(data)
          X_f = scaled_data.drop(drop_metric, axis=1)
          y_f = scaled_data[['Impressions']]
          x = X_f.values
          y = y_f.values
          
          X_tr = pd.DataFrame(x)
          X_train, X_test, y_train, y_test = train_test_split(X_tr, y, test_size=0.2, random_state=42)
          X_train.columns = range(X_train.shape[1])  
          X_train = X_train.reset_index(drop=True).as_matrix()
          y_train = y_train.reshape(len(X_train),1)
          X_test.columns = range(X_test.shape[1]) 
          X_test = X_test.reset_index(drop=True).as_matrix()
          y_test = y_test.reshape(len(X_test),1)
        
        elif list_sp[1]=='Conversions':
          
          scaled_data = minmax_scaler(data)
          X_f = scaled_data.drop(drop_metric, axis=1)
          y_f = scaled_data[['Conversions']]
          x = X_f.values
          y = y_f.values
        
          X_tr = pd.DataFrame(x)
          X_train, X_test, y_train, y_test = train_test_split(X_tr, y, test_size=0.2, random_state=42)
          X_train.columns = range(X_train.shape[1])  
          X_train = X_train.reset_index(drop=True).as_matrix()
          y_train = y_train.reshape(len(X_train),1)
          X_test.columns = range(X_test.shape[1]) 
          X_test = X_test.reset_index(drop=True).as_matrix()
          y_test = y_test.reshape(len(X_test),1)
          
        return X_train, y_train, X_test, y_test
    
    
    def maxvalue(self, list_sp):
        data = self.clean()[self.clean().Keyword_ID == list_sp[0]]
        if list_sp[1]=='Clicks':
          return data.pop('Clicks').max()
        elif list_sp[1]=='Impressions':
          return data.pop('Impressions').max()
        elif list_sp[1]=='Conversions':
          return data.pop('Conversions').max()
        
 
 
 
 
 

ls1 = ['310921431108','Clicks']
ls2 = ['28543594052', 'Impressions']

c = Cleaner(data_)
df = c.clean()
dt = minmax_scaler(df)
print (c.list_keyword[0:11])
print (c.list_KID[0:11])
print (c.hd_KID(ls2))
X_train, y_train, X_test, y_test = c.data_train(ls1, drop_metric=dropped_metrics, test_size = 0.1)
print ('X_train : ', X_train)
print ('Y_train : ', y_train)

print ('Nilai maximum : ', c.maxvalue(['3535566232','Clicks']))

    






